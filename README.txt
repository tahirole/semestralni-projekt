In order to run this project, you have to create a virtualenv (or conda) environment that will contain all libraries with exact versions specified in requirements.txt file. Then, install Jupyter Notebook or JupyterLab for running .ipynb files.

For Selenium to operate properly, you have to download a Chrome driver compatible with your Google Chrome version. Otherwise you will need to change the code, as there is explicitly ChromeDriver imported. Drivers can be found here: https://chromedriver.chromium.org/

Folders dataset and dataset_parsed are intended for Sreality backup, so that in case the html structure of current pages changes, students will be still able to process it. These folders are available only on author`s GitLab due to their big size. 
The link to GitLab: https://gitlab.fel.cvut.cz/tahirole/semestralni-projekt